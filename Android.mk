LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := AdbTest

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_RESOURCE_DIR := \
    $(LOCAL_PATH)/res

LOCAL_USE_AAPT2 := true

LOCAL_STATIC_ANDROID_LIBRARIES := \
    android-support-v4 \

LOCAL_CERTIFICATE := platform

LOCAL_AAPT_FLAGS += --version-name "1"
LOCAL_AAPT_FLAGS += --version-code 1

include $(BUILD_PACKAGE)
