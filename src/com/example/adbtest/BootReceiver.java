package com.example.adbtest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author pavelsalomatov
 * created on 18.10.18
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //need to enable receivers to prevent state when storage will be mounted when we do our check
        AdbHelper.enableReceivers(context, true);

        //check all volumes because we don't receive VOLUME_STATE_CHANGED events before phase PHASE_BOOT_COMPLETED
        Intent serviceIntent = new Intent(context, FileCheckService.class);
        serviceIntent.setAction(FileCheckService.ACTION_CHECK_ALL_DIRS);
        context.startService(serviceIntent);
    }
}
