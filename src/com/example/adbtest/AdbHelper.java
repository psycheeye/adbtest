package com.example.adbtest;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.provider.Settings;

/**
 * @author pavelsalomatov
 * created on 18.10.18
 */
public class AdbHelper {

    private static final String KEY_NEED_RETURN_SETTING = "key_need_return_setting";

    public static void enableADB(Context context, boolean enable) {
        Settings.Global.putString(context.getContentResolver(), Settings.Global.ADB_ENABLED, enable ? "1" : "0");
        if (enable) {
            setNeedReturnSettingAfterReboot(context);
        }
        //if we turn on adb receivers doesn't needed anymore
        //if we turn off adb we need to wait for new mount
        enableReceivers(context, !enable);
    }

    public static boolean isADBEnabled(Context context) {
        String enabled = Settings.Global.getString(context.getContentResolver(), Settings.Global.ADB_ENABLED);
        return "1".equals(enabled);
    }

    public static void setNeedReturnSettingAfterReboot(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(KEY_NEED_RETURN_SETTING, true).commit();
    }

    public static boolean isNeedReturnSettingAfterReboot(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(KEY_NEED_RETURN_SETTING, false);
    }

    public static void enableReceivers(Context context, boolean enabled) {
        PackageManager pm = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, VolumeStateChangeBroadcastReceiver.class);
        pm.setComponentEnabledSetting(
                componentName,
                enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
