package com.example.adbtest;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.List;

/**
 * @author pavelsalomatov
 * created on 17.10.18
 */
public class FileCheckService extends IntentService {

    private static final boolean DEBUG = false;
    private static final String TAG = FileCheckService.class.getSimpleName();

    /*package*/ static final String EXTRA_KEY_PATH = "key_path";
    /*package*/ static final String ACTION_CHECK_DIR = "action_check_dir";
    /*package*/ static final String ACTION_CHECK_ALL_DIRS = "action_check_all_dirs";

    private static final String NEEDED_FILE_NAME = "/vendor_adb_enable";

    private Handler mHandler;

    public FileCheckService() {
        super(FileCheckService.class.getSimpleName());
        setIntentRedelivery(false);
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            if (DEBUG) {
                Log.d(TAG, "onHandleIntent: intent.getAction(): " + intent.getAction());
            }
            switch (intent.getAction()) {
                case ACTION_CHECK_DIR:
                    checkPath(getApplicationContext(), intent.getStringExtra(EXTRA_KEY_PATH));
                    break;
                case ACTION_CHECK_ALL_DIRS:
                    boolean isFileFound = false;
                    Context context = getApplicationContext();

                    StorageManager storageManager = context.getSystemService(StorageManager.class);

                    List<VolumeInfo> volumeList = storageManager.getVolumes();

                    if (DEBUG) {
                        Log.d(TAG, "onHandleIntent: volumeList.size(): " + volumeList.size());
                    }

                    for (VolumeInfo volumeInfo : volumeList) {
                        if (volumeInfo.getType() == VolumeInfo.TYPE_PUBLIC && volumeInfo.isMountedReadable()) {
                            if (checkPath(context, volumeInfo.getPath().getPath())) {
                                isFileFound = true;
                                break;
                            }
                        }
                    }

                    if (!isFileFound && AdbHelper.isNeedReturnSettingAfterReboot(context)) {
                        AdbHelper.enableADB(context, false);
                    }
            }

        }
    }

    private boolean checkPath(final Context context, String path) {
        if (path != null) {
            File checkFile = new File(path + NEEDED_FILE_NAME);
            if (DEBUG) {
                Log.d("adbt FileCheckService", "checkPath: " + checkFile.getPath());
            }
            if (checkFile.exists() && checkFile.isFile()) {
                if (!AdbHelper.isADBEnabled(context)) {
                    AdbHelper.enableADB(context, true);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, R.string.adb_enabled, Toast.LENGTH_LONG).show();
                        }
                    });
                }
                return true;
            }
        }
        return false;
    }
}
