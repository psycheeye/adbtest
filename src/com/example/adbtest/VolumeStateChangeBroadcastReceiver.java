package com.example.adbtest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;

import java.io.File;

/**
 * @author pavelsalomatov
 * created on 17.10.18
 */
public class VolumeStateChangeBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (VolumeInfo.ACTION_VOLUME_STATE_CHANGED.equals(intent.getAction())) {
            final int state = intent.getIntExtra(VolumeInfo.EXTRA_VOLUME_STATE, -1);
            if (state == VolumeInfo.STATE_MOUNTED
                    || state == VolumeInfo.STATE_MOUNTED_READ_ONLY) {

                final String volumeId = intent.getStringExtra(VolumeInfo.EXTRA_VOLUME_ID);

                StorageManager storageManager = context.getSystemService(StorageManager.class);

                final VolumeInfo vol = storageManager.findVolumeById(volumeId);

                //isMountedReadable is redundant but do additional check
                if (vol != null
                        && vol.getType() == VolumeInfo.TYPE_PUBLIC
                        && vol.isMountedReadable()) {
                    //volume mounted
                    File path = vol.getPath();
                    if (path != null) {
                        Intent serviceIntent = new Intent(context, FileCheckService.class);
                        serviceIntent.setAction(FileCheckService.ACTION_CHECK_DIR);
                        serviceIntent.putExtra(FileCheckService.EXTRA_KEY_PATH, path.getPath());
                        context.startService(serviceIntent);
                    }
                }
            }
        }
    }
}
